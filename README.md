# Covid19 Face Mask Developments

With the current corona crisis there is a potential need for low cost & locally produced protection equipment. Makers to the rescue!! As part of the project [Schone Bakkes Den Haag](https://www.facebook.com/Schone-Bakkes-107525774218986) I started working on the production of 3d printed spit masks. 

*Disclaimer: I focused on creating the prototype. I didn't take the time create fancy pictures or visuals...*

![](media/covid19-facemask-avdp-6f0ec020-6f7b-4395-b54c-97fab83497a6.jpg)

## Starting position 
[wip]

## Development 
[wip]

### Development - Day 1
However, this didn't feel right. Is 3d printing really needed? It is slow and more suitable for other producs. Not flat objects. So I created a lasercut version. Much better I believe: 10 seconds production time! and is working very good. 

### Development - Day 2
However, this still didn't feel right. It could be created a lot easier. What is you use rubber band and a strip of rigid plastic? 
It works. 

## Use case and possibilities
'Target groups': 

  1. High priority medical 
  2. Medical generic 
  3. Medical care general (home, nursing homes etc )
  4. Primary occupations: pizza delivery, grocery store etc 
  5. Personal use

This is focused on general medical care and primary occupations. DIY version is for personal use. 


## How to make a mask 
### Tools and materials 

  * A4 (or less) acrylic sheet 
  * 35x3 cm flexible 1mm sheet 
  * 25cm Double sided tape
  * Sewing machine / Strong stapler / Button puncher 

### Fabrication 
[wip]

### Use 
[wip]

